# README #


### What is this repository for? ###

* Quick summary

The demo project for statsforvaltningen


### How do I get set up? ###

* Summary of set up

This is Umbraco 7.5.3.

Do these steps:

1.  Download the source

2. Add this line to your host file

          127.0.0.1 statsforvaltningendemo.com


3. Open IIS and create a new website called statsforvaltningendemo.com with binding to statsforvaltningendemo.com

4. open [http://statsforvaltningendemo.com/umbraco](Link URL)

use this username and password to login:

* username: dtl@insilico.dk
* password: Velvet007


* Configuration

* Database configuration

A backup of the data base can be find inside the repository

To connect to test server, use the following:

* server=192.168.168.109
* database=Statsforvaltningen.Demo
* user id=StatsforvaltningenDemo
* password=0



### Who do I talk to? ###

* Daniel Tabor  dlt@insilico.dk 
* Ashkan Sirous ash@insilico.dk @ashkansirous